﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L5_Blog_Console
{
    class Program 
    {
        static void Main(string[] args)
        {
            /*
            HelloWriter myHelloWriter = null;
            try
            {
                myHelloWriter = new HelloWriter("E:\\abc.txt");
                myHelloWriter.WriteHello();
                myHelloWriter.WriteHello();
                myHelloWriter.WriteHello();
                myHelloWriter.WriteHello();
                myHelloWriter.WriteHello();
                myHelloWriter.WriteHello();
                myHelloWriter.WriteHello();
                myHelloWriter.WriteHello();
                myHelloWriter.WriteHello();
                myHelloWriter.WriteHello();
                myHelloWriter.WriteHello();
                myHelloWriter.WriteHello();

            }
            finally
            {
                if (myHelloWriter != null)
                {
                    myHelloWriter.Close();
                }
            }
            */

            using (var myHelloWriter = new HelloWriter("abc.txt"))
            {
                myHelloWriter.WriteHello();
                myHelloWriter.WriteHello();
            }

            Blog blog = new Blog();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("New belarussian blog.");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("NO POLITICS. NO CURRENCY. NO GROUPS OF PEOPLE. NO CLAPS\n\n");
            do
            {
                // TODO work with file

                //Console.CursorVisible = false;
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.WriteLine("Don't be afraid if you do not see input text");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("POST:");
                Console.ForegroundColor = ConsoleColor.Black;
                string inputTextPost = Console.ReadLine();
                string author = Console.ReadLine();
                Console.ForegroundColor = ConsoleColor.Yellow;

                Post newPost = new Post(author, inputTextPost);
                Console.WriteLine("{0}\n{1,60}\n{2,60}\n\n", newPost.Text, newPost.Author, newPost.TimeAndDate);

                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("COMMENTS:");

                // TODO enum with different names and random selection
                Console.ForegroundColor= ConsoleColor.Black;
                string inputTextComment =  Console.ReadLine();
                string commentAuthor = Console.ReadLine();
                Console.ForegroundColor = ConsoleColor.Magenta;

                Comment commment = new Comment(commentAuthor, inputTextComment);
                for (int i = 0; i < 10; i++)
                {
                    newPost.AddComment(commment);
                }
                foreach (var item in newPost.ListOfComments)
                {
                    Console.WriteLine("{2}\n\t{0}\t{1}", item.Text, item.TimeAndDate, item.Author);
                }
            } while (Console.ReadLine().ToLower() != "exit");
        }
    }
}
