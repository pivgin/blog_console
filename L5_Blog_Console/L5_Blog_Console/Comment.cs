﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L5_Blog_Console
{
    public class Comment
    {
        //private string _text;
        private DateTime _timeanddate;
        private string _author;

        public Comment(string author,string text)
        {
            this.Author = author;
            this.Text = text;
        }

        public string Author {
            get { return this._author; }
            set { this._author = value; }
        }

        public DateTime TimeAndDate {
            get { return this._timeanddate = DateTime.Now; }
        }
        public string Text { get; set; }
    }
}
