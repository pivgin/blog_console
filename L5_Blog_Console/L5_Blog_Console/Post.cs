﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace L5_Blog_Console
{
    public class Post
    {
        private DateTime _timeanddate;
        private string _text;

        public Post(string author, string text)
        {
            this._text = text;
            this.Author = author;
        }

        public List<Comment> ListOfComments = new List<Comment>();

        public string Text
        {
            get { return this._text.ToUpper(); }
            set { this._text = value; }
        }

        public string Author { get; set; }

        public DateTime TimeAndDate
        {
            get { return this._timeanddate = DateTime.Now; }
        }

        public void AddComment(Comment comment)
        {
            ListOfComments.Add(comment);
        }
    }
}
