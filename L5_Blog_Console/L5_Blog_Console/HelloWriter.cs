﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L5_Blog_Console
{
    public class HelloWriter : IDisposable
    {
        private StreamWriter fileStream;
        public HelloWriter(string filename)
        {
            fileStream = new StreamWriter(filename);
        }

        public void WriteHello()
        {
            fileStream.WriteLine("Hello");
        }

        public void Close()
        {
            fileStream.Close();
        }

        public void Dispose()
        {
            Close();
        }
    }
}
